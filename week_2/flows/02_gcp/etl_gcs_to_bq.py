from pathlib import Path
from typing import List

import pandas as pd
from prefect import flow, task
from prefect_gcp.cloud_storage import GcsBucket
from prefect_gcp import GcpCredentials


@task(retries=3)
def extract_from_gcs(color: str, year: int, month: int) -> Path:
    """Download trip data from GCS"""
    gcs_path = f"data/{color}/{color}_tripdata_{year}-{month:02}.parquet"
    gcs_block = GcsBucket.load("zoom-gcs")
    gcs_block.get_directory(from_path=gcs_path, local_path=f"./")
    return Path(f"./{gcs_path}")


@task()
def write_bq(path: Path) -> None:
    """Write DataFrame to BiqQuery"""

    gcp_credentials_block = GcpCredentials.load("zoom-gcp-creds")

    df = pd.read_parquet(path)
    print(f"Number of rows: {df.shape[0]}")

    df.to_gbq(
        destination_table="dezoomcamp.rides",
        project_id="dtc-de-2023-376117",
        credentials=gcp_credentials_block.get_credentials_from_service_account(),
        chunksize=500_000,
        if_exists="append",
    )


@flow()
def etl_gcs_to_bq(color: str, year: int, month: int):
    """Main ETL flow to load data into Big Query"""
    path = extract_from_gcs(color, year, month)
    write_bq(path)


@flow()
def main_flow(
    color: str = "yellow",
    year: int = 2019,
    months: list[int] = [2, 3]
):
    for month in months:
        etl_gcs_to_bq(color, year, month)


if __name__ == "__main__":
    main_flow()
