# Question 1

**Answers:**
    --iidfile string


```bash
docker build --help | grep 'Write the image ID to the file'
```

# Question 2

**Answers:**
    3

# Question 3

**Answer:**
    20530

```sql
select
	count(1)
from green_taxi_data
where 0=0
	and DATE(lpep_pickup_datetime) = TO_DATE('2019-01-15','YYYY-MM-DD')
	and DATE(lpep_dropoff_datetime) = TO_DATE('2019-01-15','YYYY-MM-DD')
;
```

# Question 4

**Answer:**
    2019-01-15

```sql
select
	dt,
	dist
from (
	select
		dt,
		max(trip_distance) as dist
	from (
		select *,
			DATE(lpep_pickup_datetime) as dt
		from green_taxi_data
	) as t
	group by
		dt
) as t
ORDER BY dist DESC
```

# Question 5

**Answer:**
    2: 1282 ; 3: 254

```sql
select
	passenger_count,
	count(1) as cnt
from green_taxi_data
where 0=0
	and DATE(lpep_pickup_datetime) = TO_DATE('2019-01-01','YYYY-MM-DD')
	and passenger_count between 2 and 3
group by
	passenger_count
;
```

# Question 6

**Answers:**
    Long Island City/Queens Plaza

```sql
with joined as (
	select l.*, r2."Zone" as do_zone
	from green_taxi_data as l
	join zones as r1
		on l."PULocationID" = r1."LocationID"
	join zones as r2
		on l."DOLocationID" = r2."LocationID"
	where r1."Zone" like 'Astoria'
), grby as (
	select
		t.* ,
		ROW_NUMBER() over (ORDER BY tip DESC) as rnk
	from (
		select
			do_zone,
			max(tip_amount) as tip
		from joined
		group by do_zone
	) as t
)
select * from grby
-- order by rnk
where rnk = 1
```
